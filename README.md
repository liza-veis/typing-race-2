## Downloading

```
git clone https://liza-veis@bitbucket.org/liza-veis/typing-race.git
git checkout develop
cd typing-race
npm install
```

## Development

`npm start`

## Production

```
npm run build
node .
```

## Linting

`npm run lint`
