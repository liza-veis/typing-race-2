import { Socket } from 'socket.io-client';
import { EmittedEvents } from 'scripts/constants';

export class BaseController {
  private socket: Socket;

  constructor(socket: Socket) {
    this.socket = socket;
  }

  protected send<T>(event: EmittedEvents, ...props: (T | number)[]): void {
    this.socket.emit(event, ...props);
  }
}
