import { Socket } from 'socket.io-client';
import { EmittedEvents } from 'scripts/constants';
import { BaseController } from './baseController';

export class RoomController extends BaseController {
  constructor(socket: Socket) {
    super(socket);
  }

  setReadyStatus(roomName: string): void {
    this.send(EmittedEvents.SET_READY_STATUS, roomName);
  }

  setProgress(roomName: string, progress: number): void {
    this.send(EmittedEvents.SET_PROGRESS, roomName, progress);
  }
}
