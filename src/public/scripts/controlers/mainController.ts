import { Socket } from 'socket.io-client';
import { EmittedEvents } from 'scripts/constants';
import { MainService } from 'scripts/services/mainService';
import { BaseController } from './baseController';

export class MainController extends BaseController {
  private service = new MainService();

  constructor(socket: Socket) {
    super(socket);
  }

  createRoom(): void {
    const name = this.service.getRoomName();
    if (name) {
      this.send(EmittedEvents.CREATE_ROOM, name);
    }
  }

  joinRoom(roomName: string): void {
    this.send(EmittedEvents.JOIN_ROOM, roomName);
  }

  leaveRoom(roomName: string): void {
    this.send(EmittedEvents.LEAVE_ROOM, roomName);
  }
}
