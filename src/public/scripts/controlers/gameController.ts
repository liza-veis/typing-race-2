import { Socket } from 'socket.io-client';
import { EmittedEvents, GameState } from 'scripts/constants';
import { BaseController } from './baseController';
import { GameService } from '../services/gameService';

export class GameController extends BaseController {
  private service = new GameService();

  private state: GameState = GameState.FINISHED;

  constructor(socket: Socket) {
    super(socket);
  }

  prestartGame(roomName: string): void {
    this.setState(GameState.PRESTARTED);

    this.send(EmittedEvents.PRESTART_GAME, roomName);
  }

  startGame(onKeyup: (ev: KeyboardEvent) => void): void {
    this.setState(GameState.STARTED);

    this.service.startGame(onKeyup);
  }

  finishGame(roomName: string): void {
    this.setState(GameState.FINISHED);

    this.service.finishGame();
    this.send(EmittedEvents.FINISH_GAME, roomName);
  }

  getText(textId: number): Promise<string> {
    return this.service.getText(textId);
  }

  isGamePrestarted(): boolean {
    return this.state === GameState.PRESTARTED;
  }

  isGameStarted(): boolean {
    return this.state === GameState.STARTED;
  }

  isGameFinished(): boolean {
    return this.state === GameState.FINISHED;
  }

  private setState(state: GameState) {
    if (state !== this.state) {
      this.state = state;
    }
  }
}
