export class GameService {
  private onKeyup: ((ev: KeyboardEvent) => void) | null = null;

  startGame(onKeyup: (ev: KeyboardEvent) => void): void {
    this.onKeyup = onKeyup;
    window.addEventListener('keyup', this.onKeyup);
  }

  finishGame(): void {
    if (this.onKeyup) {
      window.removeEventListener('keyup', this.onKeyup);
      this.onKeyup = null;
    }
  }

  async getText(textId: number): Promise<string> {
    const data = await fetch(`/game/texts/${textId}`);

    return data.text();
  }
}
