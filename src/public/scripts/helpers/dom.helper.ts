import _ from 'lodash';

type Properties = {
  [key: string]: string | number;
};

type CreateElementProps = {
  tagName?: string;
  className?: string;
  attributes?: { [name: string]: string };
};

export const createElement = ({
  tagName = 'div',
  className,
  attributes = {},
}: CreateElementProps): HTMLElement => {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
  }

  _.toPairs(attributes).forEach(([key, value]) => element.setAttribute(key, value));

  return element;
};

export const formatClassNames = (className: string): string[] =>
  className.split(' ').filter(Boolean);

export const setProperties = (element: HTMLElement, properties: Properties): HTMLElement =>
  _.merge(element, properties);

export const setStyle = (element: HTMLElement, properties: Properties): void => {
  _.merge(element.style, properties);
};

const showElement = _.partial(setStyle, _, { display: 'block' });

const hideElement = _.partial(setStyle, _, { display: 'none' });

export const showElements = (...elements: HTMLElement[]): HTMLElement[] =>
  _.each(elements, showElement);

export const hideElements = (...elements: HTMLElement[]): HTMLElement[] =>
  _.each(elements, hideElement);
