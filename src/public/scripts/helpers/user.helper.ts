import _ from 'lodash';
import { UserItemElementProps } from 'scripts/components/UserItem';

export const isUserReady = (username: string, users: UserItemElementProps[]): boolean => {
  return Boolean(_.find(users, { name: username, isReady: true }));
};

export const addFlagToActiveUser = (
  username: string,
  users: UserItemElementProps[]
): UserItemElementProps[] => {
  const activeUser = _.find(users, { name: username });
  if (!activeUser) return users;

  const restUsers = _.reject(users, { name: username });

  return restUsers.concat({ ...activeUser, isActive: true });
};
