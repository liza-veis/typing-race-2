export const getProgress = (value: number, maxValue: number): number =>
  Math.min(Math.round((value / maxValue) * 100), 100);
