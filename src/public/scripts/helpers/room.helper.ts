import { RoomElementProps } from 'scripts/components/Room';
import { Room } from 'scripts/types';

export const mapRoom = (
  { name, usersConnected }: Room,
  onJoinRoom: (roomName: string) => void
): RoomElementProps => ({
  name,
  usersConnected,
  onJoinRoom,
});
