import { roomNameElement, leaveRoomButton, usersContainer, gamePageElement } from '../elements';
import { createUserItem, UserItemElementProps } from 'scripts/components/UserItem';
import { hideElements, setProperties, setStyle, showElements } from 'scripts/helpers/dom.helper';
import { Screen } from 'scripts/types';

export class RoomScreen implements Screen {
  set(): void {
    setStyle(gamePageElement, { display: 'flex' });
    showElements(leaveRoomButton);
  }

  reset(): void {
    hideElements(gamePageElement, leaveRoomButton);
  }

  setName(roomName: string): void {
    setProperties(roomNameElement, { textContent: roomName });
  }

  setUsers(users: UserItemElementProps[]): void {
    const userItems = users.map(createUserItem);

    setProperties(usersContainer, { innerHTML: '' });
    usersContainer.append(...userItems);
  }
}
