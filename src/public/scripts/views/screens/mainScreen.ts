import { createRoom, RoomElementProps } from 'scripts/components/Room';
import { roomsContainer, roomsPageElement } from '../elements';
import { hideElements, setProperties, showElements } from 'scripts/helpers/dom.helper';
import { Screen } from 'scripts/types';

export class MainScreen implements Screen {
  set(): void {
    showElements(roomsPageElement);
  }

  reset(): void {
    hideElements(roomsPageElement);
  }

  setRooms(rooms: RoomElementProps[]): void {
    const roomElements = rooms.map(createRoom);

    setProperties(roomsContainer, { innerHTML: '' });
    roomsContainer.append(...roomElements);
  }
}
