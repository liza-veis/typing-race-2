import _ from 'lodash';
import { commentContainer } from './elements';
import { GameState } from 'scripts/constants';
import { UserItemElementProps } from 'scripts/components/UserItem';
import { RoomElementProps } from 'scripts/components/Room';
import { MainScreen } from './screens/mainScreen';
import { RoomScreen } from './screens/roomScreen';
import { GameView } from './gameView';
import { createElement, setProperties } from 'scripts/helpers/dom.helper';
import { ScreenType } from 'scripts/types';

export class AppView {
  private gameView = new GameView();

  private screens = {
    main: new MainScreen(),
    room: new RoomScreen(),
  };

  private activeScreen: ScreenType = 'main';

  constructor() {
    this.render();
  }

  setScreen(screen: ScreenType): void {
    if (this.shouldSetScreen(screen)) {
      this.activeScreen = screen;
      this.render();
    }
  }

  setGameState(state: GameState): void {
    this.gameView.setState(state);
  }

  setRooms(rooms: RoomElementProps[]): void {
    this.screens.main.setRooms(rooms);
  }

  setRoomName(roomName: string): void {
    this.screens.room.setName(roomName);
  }

  setUsers(users: UserItemElementProps[]): void {
    this.screens.room.setUsers(users);
  }

  setReadyStatus(isReady: boolean): void {
    this.gameView.setReadyButtonStatus(isReady);
  }

  setTimer(value: number): void {
    this.gameView.setTimer(value);
  }

  setGameTime(value: number): void {
    this.gameView.setGameTime(value);
  }

  setText(text: string): void {
    this.gameView.setText(text);
  }

  resetText(): void {
    this.gameView.resetText();
  }

  fillChar(index: number): void {
    this.gameView.fillChar(index);
  }

  setComment(message: string): void {
    const wrapper = createElement({ className: 'comment-wrapper' });

    setProperties(wrapper, { innerHTML: message });
    setProperties(commentContainer, { innerHTML: '' });
    commentContainer.insertAdjacentElement('beforeend', wrapper);
  }

  private render() {
    this.getDiffScreen()?.reset();
    this.screens[this.activeScreen].set();
  }

  private shouldSetScreen(screen: ScreenType) {
    return screen in this.screens && screen !== this.activeScreen;
  }

  private getDiffScreen() {
    return _.reject(_.values(this.screens), this.activeScreen)[0];
  }
}
