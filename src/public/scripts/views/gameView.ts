import { COLOR_GREEN } from 'scripts/constants';
import {
  gameElement,
  gameTimeElement,
  gameWrapper,
  leaveRoomButton,
  readyButton,
  textContainer,
  timerElement,
} from './elements';
import { GameState } from 'scripts/constants';
import {
  createElement,
  hideElements,
  setProperties,
  setStyle,
  showElements,
} from 'scripts/helpers/dom.helper';

export class GameView {
  private state = GameState.FINISHED;

  constructor() {
    this.render();
  }

  setState(state: GameState): void {
    if (state !== this.state) {
      this.state = state;
      this.render();
    }
  }

  setReadyButtonStatus(isReady: boolean): void {
    setProperties(readyButton, { textContent: isReady ? 'Not Ready' : 'Ready' });
  }

  setTimer(value: number): void {
    setProperties(timerElement, { textContent: value.toString() });
  }

  setGameTime(value: number): void {
    setProperties(gameTimeElement, { textContent: value.toString() });
  }

  setText(text: string): void {
    const charElements = Array.from(text).map(this.createCharElement.bind(this));

    textContainer.append(...charElements);
  }

  resetText(): void {
    setProperties(textContainer, { innerHTML: '' });
  }

  fillChar(index: number): void {
    const element = textContainer.children[index] as HTMLElement;
    const nextElement = textContainer.children[index + 1] as HTMLElement;
    if (element) {
      setStyle(element, { backgroundColor: COLOR_GREEN });
    }
    if (nextElement) {
      setStyle(nextElement, { borderBottom: `1px solid ${COLOR_GREEN}` });
    }
  }

  private render(): void {
    switch (this.state) {
      case GameState.FINISHED: {
        showElements(readyButton, leaveRoomButton);
        hideElements(gameElement);
        break;
      }

      case GameState.PRESTARTED: {
        showElements(gameElement, timerElement);
        hideElements(gameWrapper, readyButton, leaveRoomButton);
        break;
      }

      case GameState.STARTED: {
        showElements(gameElement, gameWrapper);
        hideElements(timerElement, readyButton, leaveRoomButton);
        break;
      }

      default: {
        throw new Error('Wrong game state!');
      }
    }
  }

  private createCharElement(char: string) {
    const element = createElement({ tagName: 'span' });
    setProperties(element, { textContent: char });

    return element;
  }
}
