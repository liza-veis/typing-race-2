export const roomsPageElement = document.querySelector<HTMLElement>('#rooms-page')!;
export const gamePageElement = document.querySelector<HTMLElement>('#game-page')!;

export const gameWrapper = document.querySelector<HTMLElement>('.game-wrapper')!;
export const gameElement = document.querySelector<HTMLElement>('.game')!;
export const timerElement = document.querySelector<HTMLElement>('#timer')!;
export const gameTimeElement = document.querySelector<HTMLElement>('.time-left')!;
export const roomNameElement = document.querySelector<HTMLElement>('.game-title')!;

export const usersContainer = document.querySelector<HTMLElement>('.user-list')!;
export const roomsContainer = document.querySelector<HTMLElement>('.rooms-container')!;
export const textContainer = document.querySelector<HTMLElement>('#text-container')!;
export const commentContainer = document.querySelector<HTMLElement>('.comment-container')!;

export const createRoomButton = document.querySelector<HTMLElement>('#add-room-btn')!;
export const leaveRoomButton = document.querySelector<HTMLElement>('#quit-room-btn')!;
export const readyButton = document.querySelector<HTMLElement>('#ready-btn')!;
