export type User = {
  name: string;
  isReady: boolean;
  progress: number;
};

export type Room = {
  name: string;
  usersConnected: number;
  users: User[];
  canJoin: boolean;
};

export interface Screen {
  set: () => void;
  reset: () => void;
}

export type ScreenType = 'main' | 'room';
