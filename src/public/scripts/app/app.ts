import _ from 'lodash';
import { Socket } from 'socket.io-client';
import { createRoomButton, leaveRoomButton, readyButton } from 'scripts/views/elements';
import { GameState } from 'scripts/constants';
import { AppView } from 'scripts/views/appView';
import { MainController } from '../controlers/mainController';
import { RoomController } from 'scripts/controlers/roomController';
import { addFlagToActiveUser, isUserReady } from 'scripts/helpers/user.helper';
import { getProgress } from 'scripts/helpers/math.helper';
import { mapRoom } from 'scripts/helpers/room.helper';
import { GameController } from 'scripts/controlers/gameController';
import { Room, User } from 'scripts/types';

export class App {
  appView = new AppView();

  mainController: MainController;

  roomController: RoomController;

  gameController: GameController;

  activeRoomName: string | null = null;

  username: string;

  private text: string | null = null;

  private activeCharIndex = 0;

  constructor(username: string, socket: Socket) {
    this.username = username;
    this.mainController = new MainController(socket);
    this.roomController = new RoomController(socket);
    this.gameController = new GameController(socket);

    createRoomButton.addEventListener('click', this.createRoom.bind(this));
    readyButton.addEventListener('click', this.setReadyStatus.bind(this));
    leaveRoomButton.addEventListener('click', this.leaveRoom.bind(this));
  }

  createRoom(): void {
    this.mainController.createRoom();
  }

  updateRooms(rooms: Room[]): void {
    const roomsMapper = this.getRoomsMapper();
    const roomsToSet = _.filter(rooms, 'canJoin').map(roomsMapper);

    this.appView.setRooms(roomsToSet);
  }

  setActiveRoom(roomName: string): void {
    this.appView.setScreen('room');
    this.appView.setRoomName(roomName);
    this.activeRoomName = roomName;
  }

  resetActiveRoom(): void {
    this.appView.setScreen('main');
    this.appView.setRoomName('');
    this.activeRoomName = null;
  }

  joinRoom(roomName: string): void {
    if (this.activeRoomName) {
      this.leaveRoom();
    }
    this.mainController.joinRoom(roomName);
  }

  leaveRoom(): void {
    if (this.activeRoomName) {
      this.mainController.leaveRoom(this.activeRoomName);
      this.resetActiveRoom();
    }
  }

  updateRoom(room: Room): void {
    const { users } = room;
    const isReady = isUserReady(this.username, users);
    const actualUsers = addFlagToActiveUser(this.username, users);

    this.appView.setUsers(actualUsers);
    this.appView.setReadyStatus(isReady);

    this.checkGameState(room);
  }

  setReadyStatus(): void {
    if (this.activeRoomName) {
      this.roomController.setReadyStatus(this.activeRoomName);
    }
  }

  prestartGame(): void {
    if (this.activeRoomName) {
      this.gameController.prestartGame(this.activeRoomName);
      this.appView.setGameState(GameState.PRESTARTED);
    }
  }

  startGame(): void {
    const isGamePrestarted = this.gameController.isGamePrestarted();

    if (isGamePrestarted && this.text) {
      this.gameController.startGame(this.onKeyup.bind(this));
      this.appView.setGameState(GameState.STARTED);
      this.appView.setText(this.text);
      this.appView.fillChar(this.activeCharIndex - 1);
    }
  }

  finishGame(): void {
    const isGameStarted = this.gameController.isGameStarted();

    if (isGameStarted && this.activeRoomName) {
      this.gameController.finishGame(this.activeRoomName);
      this.appView.setGameState(GameState.FINISHED);
      this.appView.resetText();

      this.activeCharIndex = 0;
      this.text = '';
    }
  }

  setTimer(value: number): void {
    this.appView.setTimer(value);
  }

  setGameTime(value: number): void {
    this.appView.setGameTime(value);
  }

  async setText(textId: number): Promise<void> {
    this.text = await this.gameController.getText(textId);
  }

  setComment(message: string): void {
    this.appView.setComment(message);
  }

  canStartGame(users: User[]): boolean {
    return _.every(users, 'isReady');
  }

  canFinishGame(users: User[]): boolean {
    return _.every(users, { progress: 100 });
  }

  private checkGameState(room: Room) {
    const { users } = room;
    const isGameFinished = this.gameController.isGameFinished();
    const isGameStarted = this.gameController.isGameStarted();
    const canStartGame = this.canStartGame(users);
    const canFinishGame = this.canFinishGame(users);

    if (isGameFinished && canStartGame) {
      this.prestartGame();
    }
    if (isGameStarted && canFinishGame) {
      this.finishGame();
    }
  }

  private onKeyup(ev: KeyboardEvent) {
    if (!this.text || !this.activeRoomName) return;
    if (ev.key !== this.text[this.activeCharIndex]) return;

    this.appView.fillChar(this.activeCharIndex);

    this.activeCharIndex += 1;
    const progress = getProgress(this.activeCharIndex, this.text.length);
    this.roomController.setProgress(this.activeRoomName, progress);
  }

  private getRoomsMapper() {
    const onJoinRoom = (roomName: string) => this.mainController.joinRoom(roomName);
    return _.partial(mapRoom, _, onJoinRoom);
  }
}
