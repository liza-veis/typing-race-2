import { createElement, setProperties } from '../helpers/dom.helper';

export type RoomElementProps = {
  name: string;
  usersConnected: number;
  onJoinRoom: (name: string) => void;
};

export const createRoom = ({ name, usersConnected, onJoinRoom }: RoomElementProps): HTMLElement => {
  const roomElement = createElement({
    className: 'room',
    attributes: { id: name },
  });

  const usersConnectedWrapper = createElement({
    tagName: 'i',
    className: 'users-connected',
  });

  const usersConnectedElement = createElement({
    tagName: 'span',
  });

  const roomNameElement = createElement({
    tagName: 'h3',
    className: 'room-name',
  });

  const joinButton = createElement({
    tagName: 'button',
    className: 'join-btn',
  });

  joinButton.addEventListener('click', () => onJoinRoom(name));

  setProperties(joinButton, { textContent: 'Join' });
  setProperties(usersConnectedElement, { textContent: usersConnected.toString() });
  setProperties(roomNameElement, { textContent: name });

  usersConnectedWrapper.append(usersConnectedElement);
  usersConnectedWrapper.insertAdjacentHTML('beforeend', 'users connected');

  roomElement.append(usersConnectedWrapper, roomNameElement, joinButton);

  return roomElement;
};
