import { COLOR_GREEN, COLOR_GREEN_LIGHT } from '../constants';
import { createElement, setProperties, setStyle } from '../helpers/dom.helper';

export type UserItemElementProps = {
  name: string;
  isReady: boolean;
  progress: number;
  isActive?: boolean;
};

export const createUserItem = ({
  name: username,
  isReady,
  isActive,
  progress,
}: UserItemElementProps): HTMLElement => {
  const userItemElement = createElement({
    tagName: 'li',
    className: 'user-item',
    attributes: { id: username },
  });

  const readyStatus = createElement({
    className: `ready-status ready-status-${isReady ? 'green' : 'red'}`,
  });

  const usernameElement = createElement({
    tagName: 'span',
  });

  const userProgress = createElement({
    className: `user-progress ${username}`,
  });

  const userProgressValue = createElement({
    className: `user-progress-value`,
  });

  const isDone = progress === 100;

  setStyle(userProgressValue, {
    width: `${progress}%`,
    backgroundColor: isDone ? COLOR_GREEN_LIGHT : COLOR_GREEN,
  });

  setProperties(usernameElement, {
    textContent: `${username} ${isActive ? '(you)' : ''}`,
  });

  userProgress.append(userProgressValue);
  userItemElement.append(readyStatus, usernameElement, userProgress);

  return userItemElement;
};
