import { io } from 'socket.io-client';
import { ListenedEvents } from 'scripts/constants';
import { App } from './app/app';
import { Room } from './types';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

if (username) {
  const socket = io('', { query: { username } });

  const app = new App(username, socket);

  const onUsernameExists = () => {
    window.alert('Username already exists!');
    sessionStorage.removeItem('username');
    window.location.replace('/login');
  };

  const onRoomExists = () => {
    window.alert('Room with the same name already exists!');
  };

  const onUpdateRooms = (rooms: Room[]) => app.updateRooms(rooms);

  const onUpdateRoom = (room: Room) => app.updateRoom(room);

  const onCreateRoomDone = (roomName: string) => app.joinRoom(roomName);

  const onJoinRoomDone = (roomName: string) => app.setActiveRoom(roomName);

  const onUpdateTimer = (value: number) => app.setTimer(value);

  const onStartGame = () => app.startGame();

  const onUpdateGameTime = (value: number) => app.setGameTime(value);

  const onFinishGameTime = () => app.finishGame();

  const onSetText = (textId: number) => app.setText(textId);

  const onCommentMessage = (message: string) => app.setComment(message);

  socket.on(ListenedEvents.USERNAME_EXISTS, onUsernameExists);
  socket.on(ListenedEvents.ROOM_EXISTS, onRoomExists);
  socket.on(ListenedEvents.UPDATE_ROOMS, onUpdateRooms);
  socket.on(ListenedEvents.UPDATE_ROOM, onUpdateRoom);
  socket.on(ListenedEvents.CREATE_ROOM_DONE, onCreateRoomDone);
  socket.on(ListenedEvents.JOIN_ROOM_DONE, onJoinRoomDone);
  socket.on(ListenedEvents.UPDATE_TIMER, onUpdateTimer);
  socket.on(ListenedEvents.START_GAME, onStartGame);
  socket.on(ListenedEvents.TEXT_ID, onSetText);
  socket.on(ListenedEvents.UPDATE_GAME_TIME, onUpdateGameTime);
  socket.on(ListenedEvents.FINISH_GAME_TIME, onFinishGameTime);
  socket.on(ListenedEvents.COMMENT_MESSAGE, onCommentMessage);
}
