import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';
import { Router } from 'express';

export default (app: Router): void => {
  app.use('/login', loginRoutes);
  app.use('/game', gameRoutes);
};
