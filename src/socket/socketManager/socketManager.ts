import { Server, Socket } from 'socket.io';
import { Commentator } from '../commentator/commentator';
import { GameController } from '../controllers/gameController';
import { RoomController } from '../controllers/roomController';
import { UserController } from '../controllers/userController';
import { GameControllerManager } from '../types';

export class SocketManager {
  roomController: RoomController;

  gameController: GameControllerManager;

  userController: UserController;

  constructor(io: Server, socket: Socket) {
    const gameController = new GameController(io, socket);
    const gameProxy = new Commentator(io, gameController);

    this.userController = new UserController(io, socket);
    this.roomController = new RoomController(io, socket);
    this.gameController = gameProxy;
  }

  handleUsernameExists(): void {
    this.userController.handleUsernameExists();
  }

  onCreateRoom(roomName: string): void {
    this.roomController.create(roomName);
  }

  onJoinRoom(username: string, roomName: string): void {
    const user = this.userController.create(username);

    this.roomController.join(user, roomName);
  }

  onLeaveRoom(username: string, roomName: string): void {
    this.roomController.leave(username, roomName);
    this.updateResults(roomName);
  }

  onSetReadyStatus(username: string, roomName: string): void {
    this.roomController.setUserReadyStatus(username, roomName);
  }

  onSetProgress(username: string, roomName: string, progress: number): void {
    this.roomController.setUserProgress(username, roomName, progress);
    this.updateResults(roomName);
  }

  onPrestartGame(roomName: string): void {
    const room = this.roomController.getOne(roomName);
    const isStarted = this.roomController.isStarted(roomName);

    if (!isStarted && room) {
      this.roomController.setupForGame(roomName);
      this.gameController.sendTextId(roomName);
      this.gameController.prestartGame(room).then(() => {
        this.gameController.startGame(roomName);
      });
    }
  }

  onFinishGame(roomName: string): void {
    const isStarted = this.roomController.isStarted(roomName);
    if (isStarted) {
      const room = this.roomController.getOne(roomName);
      if (!room) return;

      this.gameController.finishGame(room);
      this.roomController.resetAfterGame(roomName);
    }
  }

  onUserDisconnect(username: string): void {
    const room = this.roomController.getUserRoom(username);
    if (!room) return;

    this.onLeaveRoom(username, room.name);
  }

  updateUserRooms(): void {
    this.roomController.updateUserRooms();
  }

  updateResults(roomName: string): void {
    const room = this.roomController.getOne(roomName);
    if (room) {
      this.gameController.updateResults(room);
    }
  }
}
