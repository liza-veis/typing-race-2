import _ from 'lodash';
import { CARS, EXTRA_MESSAGES } from './data';
import { ResultItem, User } from '../types';

export const getRandomCar = (): string => CARS[_.random(CARS.length - 1)] || '';

export const getExtraMessage = (): string =>
  EXTRA_MESSAGES[_.random(EXTRA_MESSAGES.length - 1)] || '';

export const getAnnouncementMessage = (users: User[]): string =>
  `А тем временем, список гонщиков: 
    <ul>
      ${users
        .map(
          ({ name }, i) =>
            `<li><b>${name}</b> на <i>${getRandomCar()}</i> под номером ${i + 1}</li>`
        )
        .join('')}
    </ul>
  `;

export const getResultUsualMessage = (result: ResultItem, place: number): string => {
  if (!place) {
    return `<b>${result.username}</b> сейчас первый`;
  }
  if (place === 1) {
    return ` за ним <b>${result.username}</b>`;
  }
  if (place === 2) {
    return ` а потом <b>${result.username}</b>`;
  }
  return `<b>${result.username}</b>`;
};

export const getUsualMessage = (results: ResultItem[]): string =>
  results.map(getResultUsualMessage).join(',');

export const getResultBeforeFinishMessage = (result: ResultItem, place: number): string => {
  if (!place) {
    return `До финиша осталось совсем немного и похоже что первым его может пересечь <b>${result.username}</b>.`;
  }
  if (place === 1) {
    return `Второе место может достаться  <b>${result.username}</b>.`;
  }
  if (place === 2) {
    return `Третье  <b>${result.username}</b>. Но давайте дождемся финиша.`;
  }
  return result.username;
};

export const getBeforeFinishMessage = (results: ResultItem[]): string =>
  results.map(getResultBeforeFinishMessage).join(' ');

export const getUserFinishMessage = ({ username }: ResultItem): string =>
  `Финишную прямую пересекает <b>${username}</b>`;

export const getFinishMessage = (results: ResultItem[]): string => {
  let message = 'Финальный результат:';
  const places = ['первое', 'второе', 'третье'];

  message += `<ul>
      ${results
        .slice(0, 3)
        .map(
          ({ username, time }, i) =>
            `<li>${places[i]} место занимает <b>${username}</b> со временем <i>${time} секунд</i></li>`
        )
        .join('')}
    </ul>
  `;

  return message;
};
