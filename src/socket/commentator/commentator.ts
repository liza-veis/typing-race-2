import { Server } from 'socket.io';
import _ from 'lodash';
import { GameController } from '../controllers/gameController';
import { getMilliseconds } from '../helpers/timeHelper';
import {
  BEFORE_FINISH_PROGRESS,
  SECONDS_TO_CHANGE_MESSAGE,
  USUAL_MESSAGE_UPDATE_SECONDS,
} from './config';
import {
  getAnnouncementMessage,
  getBeforeFinishMessage,
  getExtraMessage,
  getFinishMessage,
  getUserFinishMessage,
  getUsualMessage,
} from './messagesCreator';
import { START_MESSAGE } from './data';
import { EmittedEvents } from '../constants';
import { CommentMessage, GameControllerManager, Room } from '../types';

const finishedUsers = new Set<string>();
const intervals = new Map<string, NodeJS.Timeout[]>();

export class Commentator implements GameControllerManager {
  private io: Server;

  private gameController: GameController;

  private lastMessageTime: number | null = null;

  private isPreFinished = false;

  private isStarted = false;

  constructor(io: Server, gameController: GameController) {
    this.io = io;
    this.gameController = gameController;
  }

  prestartGame(room: Room): Promise<void> {
    if (this.isStarted) return Promise.resolve();

    this.isStarted = true;
    this.send({ roomName: room.name, message: START_MESSAGE });

    const { users } = room;
    const message = getAnnouncementMessage(users);

    setTimeout(() => {
      this.send({ roomName: room.name, message });
    }, getMilliseconds(SECONDS_TO_CHANGE_MESSAGE));

    this.setInterval(
      room.name,
      () => this.checkMessages(room.name),
      getMilliseconds(USUAL_MESSAGE_UPDATE_SECONDS)
    );
    this.gameController.updateResults(room);

    return this.gameController.prestartGame(room);
  }

  startGame(roomName: string): void {
    this.setInterval(
      roomName,
      () => {
        const results = this.gameController.getResults(roomName);
        const message = getUsualMessage(results);

        this.send({ roomName, message });
      },
      getMilliseconds(USUAL_MESSAGE_UPDATE_SECONDS)
    );

    this.gameController.startGame(roomName);
  }

  finishGame(room: Room): void {
    if (!this.isStarted) return;

    room.users.forEach(({ name }) => {
      finishedUsers.delete(name);
    });

    const results = this.gameController.getResults(room.name);
    const message = getFinishMessage(results);

    this.send({ roomName: room.name, message });
    this.isStarted = false;
    this.gameController.finishGame(room);
    this.clearAll(room.name);
  }

  updateResults(room: Room): void {
    this.gameController.updateResults(room);

    const results = this.gameController.getResults(room.name);
    const { progress = 0 } = results[0] || {};
    const isBeforeFinish = progress > BEFORE_FINISH_PROGRESS;

    if (isBeforeFinish && !this.isPreFinished) {
      this.isPreFinished = true;

      const message = getBeforeFinishMessage(results);
      this.send({ roomName: room.name, message });
    }

    _.filter(results, { progress: 100 }).forEach((result) => {
      if (!finishedUsers.has(result.username)) {
        finishedUsers.add(result.username);

        const message = getUserFinishMessage(result);
        this.send({ roomName: room.name, message });
      }
    });
  }

  sendTextId(roomName: string): void {
    this.gameController.sendTextId(roomName);
  }

  private setInterval(roomName: string, callback: () => void, time: number) {
    const interval = setInterval(callback, time);
    const actualIntervals = intervals.get(roomName)?.concat(interval) || [];
    intervals.set(roomName, actualIntervals);
  }

  private clearAll(roomName: string) {
    intervals.get(roomName)?.forEach((interval) => clearInterval(interval));
    intervals.delete(roomName);
    this.isPreFinished = false;
    this.lastMessageTime = null;
  }

  private checkMessages(roomName: string) {
    if (!this.shouldSendMessage()) return;

    const message = getExtraMessage();
    this.send({ roomName, message, isExtra: true });
  }

  private shouldSendMessage(): boolean {
    if (!this.lastMessageTime) return true;

    return Date.now() - this.lastMessageTime >= getMilliseconds(SECONDS_TO_CHANGE_MESSAGE);
  }

  private send({ roomName, message, isExtra }: CommentMessage) {
    if (!this.isStarted) return;
    if (isExtra && !this.shouldSendMessage()) return;

    this.io.to(roomName).emit(EmittedEvents.COMMENT_MESSAGE, message);
    this.lastMessageTime = Date.now();
  }
}
