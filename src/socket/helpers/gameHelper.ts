import { MILLISECONDS_IN_SECOND } from '../constants';
import { ResultItem } from '../types';

export const getUserTime = (finishTime: number, startTime: number): number =>
  Math.round((finishTime - startTime) / MILLISECONDS_IN_SECOND);

export const sortResults = (results: ResultItem[]): ResultItem[] => {
  const actualResults = results.slice(0);

  actualResults.sort((a, b) => {
    if (a.progress === b.progress) {
      return a.time! - b.time!;
    }
    return b.progress - a.progress;
  });

  return actualResults;
};
