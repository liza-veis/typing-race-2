import { MILLISECONDS_IN_SECOND } from '../constants';

export const getMilliseconds = (seconds: number): number => seconds * MILLISECONDS_IN_SECOND;

export const timer = (
  startValue: number,
  onUpdate: (value: number) => void,
  onFinish: () => void
): NodeJS.Timeout | null => {
  let value = startValue;

  onUpdate(value);
  value -= 1;

  if (value <= 0) {
    onFinish();
    return null;
  }

  const interval = setInterval(() => {
    onUpdate(value);
    value -= 1;

    if (value < 0) {
      clearInterval(interval);
      onFinish();
    }
  }, MILLISECONDS_IN_SECOND);

  return interval;
};
