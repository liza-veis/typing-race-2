import { Server } from 'socket.io';
import { ListenedEvents } from './constants';
import { selectUsername } from './helpers/socketHelper';
import { checkUsername } from './helpers/userHelper';
import { SocketManager } from './socketManager/socketManager';

export default (io: Server): void => {
  io.on('connection', (socket) => {
    const manager = new SocketManager(io, socket);

    const username = selectUsername(socket);
    const isUsernameExists = checkUsername(io, socket);
    if (isUsernameExists) {
      manager.handleUsernameExists();
      return;
    }

    manager.updateUserRooms();

    const onCreateRoom = (roomName: string) => manager.onCreateRoom(roomName);

    const onJoinRoom = (roomName: string) => manager.onJoinRoom(username, roomName);

    const onLeaveRoom = (roomName: string) => manager.onLeaveRoom(username, roomName);

    const onSetReadyStatus = (roomName: string) => manager.onSetReadyStatus(username, roomName);

    const onSetProgress = (roomName: string, progress: number) =>
      manager.onSetProgress(username, roomName, progress);

    const onPrestartGame = (roomName: string) => manager.onPrestartGame(roomName);

    const onFinishGame = (roomName: string) => manager.onFinishGame(roomName);

    const onDisconnect = () => manager.onUserDisconnect(username);

    socket.on(ListenedEvents.CREATE_ROOM, onCreateRoom);
    socket.on(ListenedEvents.JOIN_ROOM, onJoinRoom);
    socket.on(ListenedEvents.LEAVE_ROOM, onLeaveRoom);
    socket.on(ListenedEvents.SET_READY_STATUS, onSetReadyStatus);
    socket.on(ListenedEvents.SET_PROGRESS, onSetProgress);
    socket.on(ListenedEvents.PRESTART_GAME, onPrestartGame);
    socket.on(ListenedEvents.FINISH_GAME, onFinishGame);
    socket.on('disconnect', onDisconnect);
  });
};
