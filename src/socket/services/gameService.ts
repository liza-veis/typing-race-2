import { GameRepository } from '../repositories/gameRepository';
import { sortResults } from '../helpers/gameHelper';
import { ResultItem, Room, TimerProps } from '../types';

export class GameService {
  private repository = new GameRepository();

  setTimer(roomName: string, timerProps: TimerProps): void {
    this.repository.setTimer(roomName, timerProps);
  }

  getResults(roomName: string): ResultItem[] {
    const results = this.repository.getResults(roomName);
    return sortResults(results);
  }

  updateResults(room: Room): void {
    const { name, users } = room;
    const actualUsers = users.map(({ name, progress }) => ({
      username: name,
      progress,
    }));

    this.repository.setResults(name, actualUsers);
  }

  clearTimer(roomName: string): void {
    this.repository.deleteTimer(roomName);
  }
}
