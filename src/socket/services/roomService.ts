import _ from 'lodash';
import { RoomRepository } from '../repositories/roomRepository';
import { Room, User } from '../types';
import { setUserProgress, toggleUserReadyStatus } from '../helpers/userHelper';

export class RoomService {
  repository = new RoomRepository();

  getAll(): Room[] {
    return this.repository.getAll();
  }

  getOne(name: string): Room | null {
    return this.repository.getOne(name);
  }

  getUserRoom(username: string): Room | null {
    const rooms = this.repository.getAll();
    return _.find(rooms, { users: [{ name: username }] }) || null;
  }

  isExists(name: string): boolean {
    return this.repository.has(name);
  }

  create(name: string): Room {
    return this.repository.create(name);
  }

  delete(name: string): void {
    return this.repository.delete(name);
  }

  addUser(user: User, name: string): void {
    const room = this.getOne(name);
    if (!room) return;

    const users = room.users.concat(user);
    this.setUsers(room, users);
  }

  set(name: string, room: Room): void {
    this.repository.set(name, room);
  }

  removeUser(username: string, name: string): void {
    const room = this.getOne(name);
    if (!room) return;

    const users = _.reject(room.users, { name: username });
    this.setUsers(room, users);
  }

  toggleUserReadyStatus(username: string, name: string): void {
    const room = this.getOne(name);
    if (!room) return;

    const users = toggleUserReadyStatus(room.users, username);
    const newRoom = { ...room, users };

    this.repository.set(name, newRoom);
  }

  setUserProgress(username: string, name: string, progress: number): void {
    const room = this.getOne(name);
    if (!room) return;

    const users = setUserProgress(room.users, username, progress);
    const newRoom = { ...room, users };

    this.repository.set(name, newRoom);
  }

  isStarted(name: string): boolean {
    return this.repository.isStarted(name);
  }

  setIsStarted(name: string, value: boolean): void {
    this.repository.setIsStarted(name, value);
  }

  private setUsers(room: Room, users: User[]) {
    const { name } = room;
    const usersConnected = users.length;
    const newRoom = { ...room, users, usersConnected };

    this.repository.set(name, newRoom);
  }
}
