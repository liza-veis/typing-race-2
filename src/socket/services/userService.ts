import { User } from '../types';

export class UserService {
  create(name: string): User {
    return {
      name,
      isReady: false,
      progress: 0,
    };
  }
}
