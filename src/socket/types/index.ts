export type User = {
  name: string;
  isReady: boolean;
  progress: number;
};

export type Room = {
  name: string;
  usersConnected: number;
  users: User[];
  canJoin: boolean;
};

export interface GameControllerManager {
  prestartGame: (room: Room) => Promise<void>;

  startGame: (roomName: string) => void;

  finishGame: (room: Room) => void;

  updateResults: (room: Room) => void;

  sendTextId: (roomName: string) => void;
}

export type TimerProps = {
  startValue: number;
  onUpdate: (value: number) => void;
  onFinish: () => void;
};

export type ResultItem = {
  username: string;
  progress: number;
  time?: number;
};

export type TimerItem = {
  start: number;
  timer: NodeJS.Timeout | null;
};

export type CommentMessage = {
  message: string;
  roomName: string;
  isExtra?: boolean;
};
