import { getUserTime } from '../helpers/gameHelper';
import { timer } from '../helpers/timeHelper';
import { ResultItem, TimerProps, TimerItem } from '../types';

const resultsDb: { [roomName: string]: ResultItem[] } = {};

const timersDb: { [roomName: string]: TimerItem } = {};

export class GameRepository {
  setTimer(roomName: string, { startValue, onUpdate, onFinish }: TimerProps): void {
    if (timersDb[roomName]) {
      this.deleteTimer(roomName);
    }

    const actualTimer = timer(startValue, onUpdate, onFinish);

    timersDb[roomName] = {
      start: Date.now(),
      timer: actualTimer,
    };
  }

  deleteTimer(roomName: string): void {
    const { timer } = timersDb[roomName] || {};
    if (timer) {
      clearInterval(timer);
      delete timersDb[roomName];
    }
  }

  getResults(roomName: string): ResultItem[] {
    const { start = 0 } = timersDb[roomName] || {};
    const results = resultsDb[roomName] || [];

    return results.map((result) => ({ time: getUserTime(Date.now(), start), ...result }));
  }

  setResults(roomName: string, results: ResultItem[]): void {
    const oldResults = resultsDb[roomName];

    resultsDb[roomName] = results.map((result) => {
      const oldResult = oldResults?.find(({ username }) => username === result.username);

      if (result.progress === 100 && !oldResult?.time) {
        const { start } = timersDb[roomName] || {};
        return { time: start ? getUserTime(Date.now(), start) : 0, ...result };
      }

      return { ...oldResult, ...result };
    });
  }
}
