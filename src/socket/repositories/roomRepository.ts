import { Room } from '../types';

const rooms = new Map<string, Room>();
const startedRooms = new Set<string>();

export class RoomRepository {
  getAll(): Room[] {
    return [...rooms.values()];
  }

  getOne(name: string): Room | null {
    return rooms.get(name) || null;
  }

  has(name: string): boolean {
    return rooms.has(name);
  }

  create(name: string): Room {
    const room = {
      name,
      usersConnected: 0,
      users: [],
      canJoin: true,
    };

    this.set(name, room);

    return room;
  }

  set(name: string, room: Room): void {
    rooms.set(name, room);
  }

  delete(name: string): void {
    rooms.delete(name);
  }

  isStarted(name: string): boolean {
    return startedRooms.has(name);
  }

  setIsStarted(name: string, value: boolean): void {
    if (value) {
      startedRooms.add(name);
    } else {
      startedRooms.delete(name);
    }
  }
}
