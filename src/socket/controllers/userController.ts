import { Server, Socket } from 'socket.io';
import { BaseController } from './baseController';
import { UserService } from '../services/userService';
import { User } from '../types';
import { EmittedEvents } from '../constants';

export class UserController extends BaseController {
  service = new UserService();

  constructor(io: Server, socket: Socket) {
    super(io, socket);
  }

  handleUsernameExists(): void {
    this.sendToUser(EmittedEvents.USERNAME_EXISTS);
  }

  create(name: string): User {
    return this.service.create(name);
  }
}
