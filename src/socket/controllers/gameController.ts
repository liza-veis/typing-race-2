import { Server, Socket } from 'socket.io';
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from '../config';
import { EmittedEvents } from '../constants';
import { GameService } from '../services/gameService';
import { BaseController } from './baseController';
import data from '../../data';
import { GameControllerManager, ResultItem, Room } from '../types';

export class GameController extends BaseController implements GameControllerManager {
  service = new GameService();

  constructor(io: Server, socket: Socket) {
    super(io, socket);
  }

  prestartGame(room: Room): Promise<void> {
    const { name: roomName } = room;
    const startValue = SECONDS_TIMER_BEFORE_START_GAME;

    return new Promise((resolve) => {
      const onUpdate = (value: number) =>
        this.sendToRoom(roomName, EmittedEvents.UPDATE_TIMER, value);

      const onFinish = () => {
        this.sendToRoom(roomName, EmittedEvents.START_GAME);
        resolve();
      };

      this.service.setTimer(roomName, { startValue, onUpdate, onFinish });
    });
  }

  startGame(roomName: string): void {
    const startValue = SECONDS_FOR_GAME;

    const onUpdate = (value: number): void =>
      this.sendToRoom(roomName, EmittedEvents.UPDATE_GAME_TIME, value);

    const onFinish = (): void => {
      this.sendToRoom(roomName, EmittedEvents.FINISH_GAME_TIME);
    };

    this.service.clearTimer(roomName);
    this.service.setTimer(roomName, { startValue, onUpdate, onFinish });
  }

  finishGame(room: Room): void {
    this.service.clearTimer(room.name);
  }

  getResults(roomName: string): ResultItem[] {
    return this.service.getResults(roomName);
  }

  updateResults(room: Room): void {
    this.service.updateResults(room);
  }

  sendTextId(roomName: string): void {
    const textId = Math.floor(Math.random() * data.texts.length);

    this.io.to(roomName).emit('TEXT_ID', textId);
  }
}
