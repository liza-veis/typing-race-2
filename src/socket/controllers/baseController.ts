import { Server, Socket } from 'socket.io';
import { EmittedEvents } from '../constants';

export class BaseController {
  io: Server;

  socket: Socket;

  constructor(io: Server, socket: Socket) {
    this.io = io;
    this.socket = socket;
  }

  protected send<T>(event: EmittedEvents, ...props: T[]): void {
    this.io.emit(event, ...props);
  }

  protected sendToUser<T>(event: EmittedEvents, ...props: T[]): void {
    this.socket.emit(event, ...props);
  }

  protected sendToRoom<T>(roomName: string, event: EmittedEvents, ...props: T[]): void {
    this.io.to(roomName).emit(event, ...props);
  }
}
