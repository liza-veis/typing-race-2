import { Server, Socket } from 'socket.io';
import { EmittedEvents } from '../constants';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';
import { RoomService } from '../services/roomService';
import { BaseController } from './baseController';
import { Room, User } from '../types';

export class RoomController extends BaseController {
  service = new RoomService();

  constructor(io: Server, socket: Socket) {
    super(io, socket);
  }

  create(name: string): void {
    const isRoomExists = this.service.isExists(name);

    if (isRoomExists) {
      this.send(EmittedEvents.ROOM_EXISTS);
      return;
    }

    this.service.create(name);
    const rooms = this.service.getAll();

    this.sendToUser(EmittedEvents.CREATE_ROOM_DONE, name);
    this.send(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  getOne(name: string): Room | null {
    return this.service.getOne(name);
  }

  delete(name: string): void {
    this.service.delete(name);
    const rooms = this.service.getAll();

    this.send(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  join(user: User, name: string): void {
    this.socket.join(name);
    this.service.addUser(user, name);

    const room = this.service.getOne(name);
    const canJoin = this.getCanJoin(name, room?.usersConnected);
    const newRoom = { ...room, canJoin };

    this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, newRoom);
    this.sendToUser(EmittedEvents.JOIN_ROOM_DONE, name);

    const rooms = this.service.getAll();
    this.send(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  leave(username: string, name: string): void {
    this.socket.leave(name);
    this.service.removeUser(username, name);

    const room = this.service.getOne(name);
    const canJoin = this.getCanJoin(name, room?.usersConnected);
    const newRoom = { ...room, canJoin };
    if (room?.usersConnected) {
      const rooms = this.service.getAll();
      this.send(EmittedEvents.UPDATE_ROOMS, rooms);
      this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, newRoom);
    } else {
      this.delete(name);
    }
  }

  setUserReadyStatus(username: string, name: string): void {
    this.service.toggleUserReadyStatus(username, name);

    const room = this.service.getOne(name);
    if (!room) return;

    this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, room);
  }

  setUserProgress(username: string, name: string, progress: number): void {
    this.service.setUserProgress(username, name, progress);

    const room = this.service.getOne(name);
    if (!room) return;

    this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, room);
  }

  setUserName(username: string, name: string, progress: number): void {
    this.service.setUserProgress(username, name, progress);

    const room = this.service.getOne(name);
    if (!room) return;

    this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, room);
  }

  getUserRoom(username: string): Room | null {
    return this.service.getUserRoom(username);
  }

  updateUserRooms(): void {
    const rooms = this.service.getAll();
    this.sendToUser(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  setupForGame(name: string): void {
    this.service.setIsStarted(name, true);

    const room = this.service.getOne(name);
    const canJoin = this.getCanJoin(name, room?.usersConnected);

    if (room) {
      const newRoom = { ...room, canJoin };
      this.service.set(name, newRoom);
      this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, newRoom);
    }

    const rooms = this.service.getAll();
    this.send(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  resetAfterGame(name: string): void {
    this.service.setIsStarted(name, false);

    const room = this.service.getOne(name);
    const canJoin = this.getCanJoin(name, room?.usersConnected);

    if (room) {
      const users = room?.users.map((user) => ({ ...user, isReady: false, progress: 0 }));
      const newRoom = { ...room, users, canJoin };
      this.service.set(name, newRoom);
      this.sendToRoom(name, EmittedEvents.UPDATE_ROOM, newRoom);
    }

    const rooms = this.service.getAll();
    this.send(EmittedEvents.UPDATE_ROOMS, rooms);
  }

  isStarted(name: string): boolean {
    return this.service.isStarted(name);
  }

  private getCanJoin(name: string, usersConnected = 0) {
    const isMaxUsers = usersConnected >= MAXIMUM_USERS_FOR_ONE_ROOM;
    const isGameStarted = this.service.isStarted(name);
    return !isMaxUsers && !isGameStarted;
  }
}
